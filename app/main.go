package main

import (
	"context"
	"encoding/xml"
	"flag"
	"fmt"
	"strconv"
	"strings"
	"time"

	"gitlab.com/karora/go50a/alog"
	"gitlab.com/karora/go50a/exporter"
	"gitlab.com/karora/go50a/g50api"
	"gitlab.com/karora/go50a/settings"
)

var (
	configFile  = flag.String("config", "", "The configuration file.")
	runExporter = flag.Bool("exporter", false, "Run as a prometheus exporter for the G50A status")
	g50_address = flag.String("g50a", "", "The address of the G50A on the network")
	listen_port = flag.Int("port", 0, "The port for the exporter to listen on")

	zones     = flag.String("zones", "", "The zones to query or set")
	queryType = flag.String("type", "", "The type of attributes to query: SystemData, SummerTime, Clock, FilterStatusList, AlarmStatusList or DdcAlarmStatusList")
	getBulk   = flag.Bool("bulk", false, "Just retrieve the Bulk attribute for the zone(s)")
	getAll    = flag.Bool("all", false, "Retrieve all attributes for the zone(s)")

	setFan         = flag.Int("fan", 0, "Set fan speed, from 1 (low) to 5 (high)")
	setMode        = flag.String("mode", "", "Set operating mode: heat, cool, dry, fan or auto")
	setTemperature = flag.Int("temp", 0, "Set the target temperature")
	setVane        = flag.Int("vane", 0, "Set the vane direction from 1 (vertical) to 4 (horizontal)")
	setOn          = flag.Bool("on", false, "Set this zone (or zones) to 'on'")
	setOff         = flag.Bool("off", false, "Set this zone (or zones) to 'off'")

	setCoolMin = flag.Int("cool_min", 0, "Set the minimum target temperature for 'COOL' mode")
	setHeatMax = flag.Int("heat_max", 0, "Set the maximum target temperature for 'HEAT' mode")
	setAutoMin = flag.Int("auto_min", 0, "Set the min temp before heating on in 'AUTO' mode")
	setAutoMax = flag.Int("auto_max", 0, "Set the max temp before cooling on in 'AUTO' mode")

	remoteOff = flag.Bool("remote_off", false, "Disable the remote control for this zone")
	remoteOn  = flag.Bool("remote_on", false, "Enable the remote control for this zone")

	debug = flag.Bool("debug", false, "Enable debugging output")
)

var generalContext context.Context

func setupLogging() {
	// log_level can be 0 (panic) through 6(trace), default is 4(info)
	if level := settings.GetInt(settings.LogLevel); level > 0 {
		alog.SetLogLevel(alog.Level(level))
	}
	if *debug {
		settings.SetDebug(true)
	}
}

func doSettings() {
	if *setFan == 0 && *setMode == "" && *setTemperature == 0 && *setVane == 0 && !*setOn && !*setOff &&
		*setHeatMax == 0 && *setCoolMin == 0 && *setAutoMin == 0 && *setAutoMax == 0 && !*remoteOff && !*remoteOn {
		return
	}
	if *zones == "" {
		alog.Fatal("You must specify the '-zones' option to apply a setting.")
	}
	if settings.Debug() {
		fmt.Println("Not doing anything about settings yet.")
	}
	req := g50api.G50Request{
		Type: g50api.RequestTypeSet,
	}
	data := g50api.G50Data{}

	for _, s := range strings.Split(*zones, ",") {
		if zoneID, _ := strconv.Atoi(s); zoneID > 0 {
			zone := g50api.Zone{
				Zone: zoneID,
			}
			zone.Mode = strings.ToUpper(*setMode)
			zone.SetTemp = g50api.IntToTemp(*setTemperature)
			zone.FanSpeed = g50api.IntToFanSpeed(*setFan)
			zone.Vane = g50api.IntToVane(*setVane)
			if *setOn {
				zone.Drive = g50api.IntToDrive(1)
			} else if *setOff {
				zone.Drive = g50api.IntToDrive(0)
			}

			if *remoteOn {
				zone.RemoCon = g50api.RemoteControlPermit
			} else if *remoteOff {
				zone.RemoCon = g50api.RemoteControlDeny
			}

			zone.HeatMax = g50api.IntToTemp(*setHeatMax)
			zone.CoolMin = g50api.IntToTemp(*setCoolMin)
			zone.AutoMin = g50api.IntToTemp(*setAutoMin)
			zone.AutoMax = g50api.IntToTemp(*setAutoMax)

			data.Zones = append(data.Zones, zone)
		}
	}

	req.Data = data
	req.Request(generalContext)
	time.Sleep(time.Second)
}

func doQueries() {
	if *zones == "" && *queryType == "" {
		if *runExporter {
			return
		}
		*zones = settings.GetString(settings.G50Zones)
	}

	req := g50api.G50Request{
		Type: g50api.RequestTypeGet,
	}
	data := g50api.G50Data{}
	if *zones != "" {
		*queryType = g50api.QueryTypeZone
	}
	if *queryType != "" {
		if _, exists := g50api.QueryTypes()[*queryType]; !exists {
			alog.Fatalf("Invalid type %q", *queryType)
		}
		switch *queryType {
		case g50api.QueryTypeZone:
			if *zones == "" {
				*zones = settings.GetString(settings.G50Zones)
				if *zones == "" {
					alog.Fatalf("You must specify either the '-zones' option or %q in the config file.", settings.G50Zones)
				}
			}
			for _, s := range strings.Split(*zones, ",") {
				if zoneID, _ := strconv.Atoi(s); zoneID > 0 {
					zoneRequest := g50api.DefaultZoneRequest(zoneID)
					if *getBulk {
						zoneRequest = g50api.BulkZoneRequest(zoneID)
					} else if *getAll {
						zoneRequest = g50api.FullZoneRequest(zoneID)
					}
					data.Zones = append(data.Zones, zoneRequest)
				}
			}
		case g50api.QueryTypeSystemData:
			data.SystemData = g50api.SystemDataRequest()
		case g50api.QueryTypeSummerTime:
			data.SummerTime = g50api.SummerTimeRequest()
		case g50api.QueryTypeClock:
			data.Clock = g50api.ClockRequest()
		case g50api.QueryTypeAlarmStatus:
			fallthrough
		case g50api.QueryTypeDdcAlarmStatus:
			fallthrough
		case g50api.QueryTypeFilterStatus:
			data.Zones = append(data.Zones, g50api.Zone{QueryType: &g50api.QueryType{XMLName: xml.Name{Local: *queryType}}})
		}
	}
	req.Data = data
	response, err := req.Request(generalContext)
	if err != nil {
		alog.Error("Error fetching response: ", err)
	}
	fmt.Println(response)
}

func loadConfig() {
	flag.Parse()
	if err := settings.LoadFromFile(*configFile); err != nil {
		alog.Fatal("Error loading settings: ", err)
	}
	if *g50_address != "" {
		settings.SetObject(settings.G50Address, *g50_address)
	}
	if *zones != "" {
		settings.SetObject(settings.G50Zones, *zones)
	}
	if *listen_port != 0 {
		settings.SetObject(settings.ServerListenPort, *listen_port)
	}
	if !settings.Exists(settings.ServerListenPort) {
		settings.SetObject(settings.ServerListenPort, 9087)
	}
	if !settings.Exists(settings.AllowedMetricIPs) {
		// At least allow from localhost by default
		settings.SetObject(settings.AllowedMetricIPs, map[string]string{"127": "Localhost"})
	}
	setupLogging()
}

func main() {
	generalContext = context.Background()

	loadConfig()
	doSettings()
	doQueries()

	if *runExporter {
		settings.SubscribeToReload(setupLogging)
		exporter.Run()
	}
}
