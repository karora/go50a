package scraper

import (
	"context"
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/karora/go50a/alog"
	"gitlab.com/karora/go50a/g50api"
	"gitlab.com/karora/go50a/settings"
)

var (
	zoneReq        []g50api.Zone
	scrapeInterval time.Duration

	setTemperature = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Namespace: "g50a",
			Subsystem: "zone",
			Name:      "set_temp",
			Help:      "The set temperature",
		},
		[]string{"zone"},
	)
	inletTemperature = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Namespace: "g50a",
			Subsystem: "zone",
			Name:      "inlet_temp",
			Help:      "The inlet temperature",
		},
		[]string{"zone"},
	)
	operating = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Namespace: "g50a",
			Subsystem: "zone",
			Name:      "operating",
			Help:      "Whether the zone is operating",
		},
		[]string{"zone", "mode", "fan", "vane"},
	)

	// Drive:        "*",
	// Mode:         "*",
	// SetTemp:      "*",
	// InletTemp:    "*",
	// FanSpeed:     "*",
	// AirDirection: "*",
)

func init() {
	prometheus.MustRegister(
		setTemperature,
		inletTemperature,
		operating,
	)
}

var firstScrapeDone = false

func InitScraper() {
	exportedZones := strings.Split(settings.GetString(settings.G50Zones), ",")
	newZoneReq := make([]g50api.Zone, 0, len(exportedZones))
	for _, z := range exportedZones {
		zone, err := strconv.Atoi(z)
		if err != nil {
			alog.Errorf("Could not parse zone as integer: %q", z)
			return
		}
		newZoneReq = append(newZoneReq, g50api.DefaultZoneRequest(zone))
	}
	if len(newZoneReq) < 1 {
		alog.Errorf("No zones configured for exporting!  Set %q in configuration file.", settings.G50Zones)
		return
	}
	zoneReq = newZoneReq
	fmt.Println(zoneReq)

	scrapeInterval = time.Duration(float64(time.Second) * settings.GetFloat64(settings.ScrapeInterval))
	if scrapeInterval < (time.Second * 30) {
		scrapeInterval = time.Second * 50
	}
	if !firstScrapeDone {
		doScrape()
		firstScrapeDone = true
	}
}

func doScrape() []g50api.Zone {
	req := g50api.G50Request{
		Type: g50api.RequestTypeGet,
		Data: g50api.G50Data{
			Zones: zoneReq,
		},
	}
	ctx, cancel := context.WithTimeout(context.Background(), scrapeInterval)
	defer cancel()

	resp, err := req.Request(ctx)
	if err != nil {
		alog.Error("Error in request: ", err.Error())
	}
	for _, z := range resp.Zones {
		zone := strconv.Itoa(z.Zone)
		temp, _ := strconv.ParseFloat(z.SetTemp, 64)
		setTemperature.WithLabelValues(zone).Set(temp)
		temp, _ = strconv.ParseFloat(z.InletTemp, 64)
		inletTemperature.WithLabelValues(zone).Set(temp)
		operating.WithLabelValues(zone, z.Mode, z.FanSpeed, z.Vane).Set(float64(g50api.DriveToInt(z.Drive)))
	}
	return resp.Zones
}

func Scraper(finished chan struct{}, postScrape func([]g50api.Zone)) {
	defer close(finished)

	var now time.Time
	for {
		now = time.Now()
		scrape := doScrape()
		if postScrape != nil {
			postScrape(scrape)
		}
		time.Sleep(scrapeInterval - time.Since(now))
	}
}
