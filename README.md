# Go50a - command-line and Prometheus export for Mitsubishi G50a

The Mistubishi G50a is a group controller for a series of Mitsubishi heat pumps, typically used in a setting where there are local heating/cooling units in (e.g.) each room which are connected to one (or more) external heat exchanger units.  In a residential setting this is often called a "Multi-Split" system.

The G50a controller sits in a centralised location, hopefully connected to your LAN (or some other private network) and monitors / controls the head units and heat exchanger units via a proprietary 'Mnet' protocol.

This program provides functionality to:

- Query the G50a controller for status of connected units
- Set various values for the connected units
- Run as an 'exporter' for "OpenMetrics" (a.k.a Prometheus) style metrics collection

## License

This code is (c) Andrew McMillan and is licensed without warranty under the GPL v3.0.

## Using the program

Simple usage of ./g50a to retrieve values from your G50a controller

1. Create your configuration file.  Typically this will be at ~/.go5a-config.json and you MUST set a value for `g50_address`, but see the full example for other configuration you may want.
2. Run the command, e.g.: `./g50a -zones 1,2` to query the state of some zones.  If you specified `g50_zones` in your config file you can omit the `-zones` option on querying.

### General Options

  -config string
     The configuration file. (default "~/.go50a-config.json")

  -debug
     Enable debugging output

  -zones string
     The zones to query or set

### Querying Information

The following options are available for querying:

  -all
     Retrieve all attributes for the zone(s)

  -bulk
     Just retrieve the Bulk attribute for the zone(s)

  -type string
     The type of attributes to query: SystemData, SummerTime, Clock, FilterStatusList, AlarmStatusList or DdcAlarmStatusList. The default is `Mnet`.

### Setting Values

When setting values it is always necessary to include the `-zones` option to specify which zones are to be modified.

  -off
     Set this zone (or zones) to 'off'

  -on
     Set this zone (or zones) to 'on'

  -mode string
     Set operating mode: heat, cool, dry, fan or auto

  -temp &lt;temperature&gt;
     Set the target temperature

  -fan int
     Set fan speed, from 1 (low) to 5 (high)

  -vane &lt;int&gt;
     Set the vane direction from 1 (vertical) to 4 (horizontal) or 5 (swing)

  -remote_off
     Disable the remote control for this zone

  -remote_on
     Enable the remote control for this zone

  -auto_max &lt;temperature&gt;
     Set the max temp before cooling on in 'AUTO' mode

  -auto_min &lt;temperature&gt;
     Set the min temp before heating on in 'AUTO' mode

  -heat_max &lt;temperature&gt;
     Set the maximum target temperature for 'HEAT' mode

  -cool_min &lt;temperature&gt;
     Set the minimum target temperature for 'COOL' mode

### Run as an 'Exporter' for collecting Prometheus-style Metrics

When you supply the `-exporter` option the program will not exit but will open a port on the configured `listen_port` and respond to http requests with a typical Prometheus format set of metrics, like:

```openmetrics
# HELP g50a_zone_inlet_temp The inlet temperature
# TYPE g50a_zone_inlet_temp gauge
g50a_zone_inlet_temp{zone="1"} 18
g50a_zone_inlet_temp{zone="2"} 18
g50a_zone_inlet_temp{zone="3"} 19
# HELP g50a_zone_operating Whether the zone is operating
# TYPE g50a_zone_operating gauge
g50a_zone_operating{fan="LOW",mode="HEAT",vane="VERTICAL",zone="2"} 0
g50a_zone_operating{fan="MID2",mode="HEAT",vane="MID2",zone="3"} 0
g50a_zone_operating{fan="MID2",mode="HEAT",vane="VERTICAL",zone="1"} 0
# HELP g50a_zone_set_temp The set temperature
# TYPE g50a_zone_set_temp gauge
g50a_zone_set_temp{zone="1"} 19
g50a_zone_set_temp{zone="2"} 18
g50a_zone_set_temp{zone="3"} 18
```

### Full Configuration File Example

```json
{
  "g50_address": "192.168.1.9",
  "g50_zones": "1,2,3",
  "scrape_interval": 50,
  "debug_mode": false,
  "listen_port": 9087,
  "log_level": 6,
  "allowed_metrics_ips": {
    "192.168": "Local RFC1918 addresses",
    "127": "Localhost",
    "172.16": "Local docker containers",
    "172.17": "Local docker containers",
    "172.18": "Local docker containers",
    "172.19": "Local docker containers",
    "172.20": "Local docker containers",
    "172.21": "Local docker containers",
    "172.22": "Local docker containers",
    "172.23": "Local docker containers",
    "172.24": "Local docker containers",
    "172.25": "Local docker containers",
    "172.26": "Local docker containers",
    "172.27": "Local docker containers",
    "172.28": "Local docker containers",
    "172.29": "Local docker containers",
    "172.30": "Local docker containers",
    "172.31": "Local docker containers"
  }
}
```
