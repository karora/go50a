// Package alog is Andrew's wrapper for logging
//
package alog

import (
	"context"
	"fmt"
	"math/rand"
	"os"
	"regexp"
	"runtime"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/rifflock/lfshook"
	"github.com/segmentio/ksuid"
	"github.com/sirupsen/logrus"
)

// GINRequestID is the key for the request ID value in the Gin context
const GINRequestID = "reqid"

// ALogContext is the key for the log context in Gin
const ALogContext = "ALogContext"

var (
	stripone   = regexp.MustCompile("^.*/go/src/")
	striptwo   = regexp.MustCompile("^.*gitlab.com/")
	stripthree = regexp.MustCompile("^.*karora/")
)

func stripFnPrefix(fn string) string {
	return stripthree.ReplaceAllString(striptwo.ReplaceAllString(stripone.ReplaceAllString(fn, ""), ""), "")
}

// Fields type, used to pass to `WithFields`.
type Fields map[string]interface{}

// Entry is a structure to contain the final or intermediate logging entry. It
// contains all the fields passed with WithField{,s}. It's finally logged when
// Debug, Customer, Print, Fatal or Panic is called on it. These objects can be
// reused and passed around as much as you wish to avoid field duplication.
type Entry struct {
	*logrus.Entry
	FieldSet map[string]interface{}
}

// Logger is the thing that logs.
type Logger struct {
	logrus.Logger
}

// Level type
type Level logrus.Level

// These are the different logging levels. You can set the logging level to log
// on your instance of logger.
const (
	// PanicLevel level, highest level of severity. Logs and then calls panic with the
	// message passed to Debug, Info, ...
	PanicLevel Level = iota
	// FatalLevel level. Logs and then calls `logger.Exit(1)`. It will exit even if the
	// logging level is set to Panic.
	FatalLevel
	// ErrorLevel level. Logs. Used for errors that should definitely be noted.
	// Commonly used for hooks to send errors to an error tracking service.
	ErrorLevel
	// WarnLevel level. Non-critical entries that deserve eyes.
	WarnLevel
	// InfoLevel level. General operational entries about what's going on inside the
	// application.
	InfoLevel
	// DebugLevel level. Usually only enabled when debugging. Very verbose logging.
	DebugLevel
	// TraceLevel level. Designates finer-grained informational events than the Debug.
	TraceLevel
)

var aLogger *Logger
var aLogEntry *Entry

func init() {
	rand.Seed(int64(time.Now().Nanosecond()))
	setDefaults()
}

func setDefaults() {
	aLogger = &Logger{
		logrus.Logger{
			Out:       os.Stderr,
			Formatter: new(logrus.TextFormatter),
			Hooks:     make(logrus.LevelHooks),
			Level:     logrus.InfoLevel,
		},
	}

	// Also set the defaults for other things that might be using it...
	logrus.SetOutput(aLogger.Out)
	logrus.SetFormatter(aLogger.Formatter)
	aLogEntry = NewEntry(aLogger)
}

// SetOutputFile sets the output to the specified file
func SetOutputFile(fileName string) {
	setDefaults()
	aLogEntry.Logger.Hooks.Add(lfshook.NewHook(
		fileName,
		&logrus.JSONFormatter{},
	))
}

// SetLogLevel sets the minimum Level to be logged
func SetLogLevel(newLevel Level) (oldLevel Level) {
	oldLevel = Level(aLogger.Level)
	aLogger.Level = logrus.Level(newLevel)
	return
}

// WithFields creates an entry from the standard logger and adds multiple
// fields to it. This is simply a helper for `WithField`, invoking it once
// for each field.
//
// Note that it doesn't log until you call Debug, Print, Info, Warn, Fatal
// or Panic on the Entry it returns.
func (e *Entry) WithFields(f Fields) *Entry {
	if e.FieldSet != nil {
		for k, v := range f {
			e.FieldSet[k] = v
		}
	} else {
		e.FieldSet = f
	}
	newEntry := &Entry{
		Entry:    logrus.WithFields(logrus.Fields(e.FieldSet)),
		FieldSet: e.FieldSet,
	}
	newEntry.Logger = e.Logger
	return newEntry
}

// WithFields creates an entry from the standard logger and adds multiple
// fields to it. This is simply a helper for `WithField`, invoking it once
// for each field.
//
// Note that it doesn't log until you call Debug, Print, Info, Warn, Fatal
// or Panic on the Entry it returns.
func WithFields(f Fields) *Entry {
	return aLogEntry.WithFields(f)
}

// NewEntry returns you a new Entry (!)
//
// it is a wrapper for logrus.NewEntry
func NewEntry(l *Logger) *Entry {
	return &Entry{
		Entry: logrus.NewEntry(&l.Logger),
	}
}

type reqIDType int

const reqIDKey reqIDType = 1

// WithContext logs within the supplied context
func WithContext(ctx context.Context) *Entry {
	f := Fields{}
	requestID, _ := ctx.Value(reqIDKey).(string)
	if requestID != "" {
		f[GINRequestID] = requestID
	} else {
		delete(f, GINRequestID)
	}
	if requestID == "" {
		return aLogEntry
	}
	return NewEntry(aLogger).WithFields(f)
}

// WithRequestID is used for adding a RequestID value onto a context
func WithRequestID(ctx context.Context) context.Context {
	var requestID string
	c, ok := ctx.(*gin.Context)
	if ok && c != nil {
		requestID = c.GetString(GINRequestID)
	} else {
		requestID = UniqueTag()
	}
	return context.WithValue(ctx, reqIDKey, requestID)
}

// RequestContext is used for retrieving a request context allowing log messages for
// a single request to be associated through WithContext
func RequestContext(c *gin.Context) context.Context {
	var requestID string
	if c != nil {
		requestID = c.GetString(GINRequestID)
	}
	return context.WithValue(context.Background(), reqIDKey, requestID)
}

// New exposes the internal logrus implementation, so you should probably
// avoid this if possible.
//
// Creates a new logger. Configuration should be set by changing `Formatter`,
// `Out` and `Hooks` directly on the default logger instance. You can also
// just instantiate your own:
//   var log = &Logger{
//	Out: os.Stderr,
//	Formatter: new(JSONFormatter),
//	Hooks: make(LevelHooks),
//	Level: logrus.DebugLevel,
//   }
// It's recommended to make this a global instance called `log`.
func New() *Logger {
	return &Logger{*logrus.New()}
}

// UniqueTag returns a globally unique log identifier which can be
// used to group log entries that come from the same source program.
func UniqueTag() string {
	return ksuid.New().String()
}

// LogStack logs the calling stack, with the associated message, in the range specified.
func LogStack(message string, from, to int) {
	aLogEntry.LogStack(message, from+1, to+1)
}

// Customer is like log.Info, but for logging customer-visible messages
func Customer(args ...interface{}) {
	aLogEntry.WithField("visibility", "customer").Info(args...)
}

// PanicLogger converts a panic into a Warn + recover
func PanicLogger() {
	if r := recover(); r != nil {
		Warn("Continuing after panic: ", r)
	}
}

// Customerf is like log.Printf, for logging customer-visible messages
func Customerf(format string, args ...interface{}) {
	aLogEntry.WithField("visibility", "customer").Infof(format, args...)
}

// Info is a wrapper for the logrus.Info function.
func Info(args ...interface{}) {
	aLogEntry.Info(args...)
}

// Infof is a wrapper for the logrus.Infof function.
func Infof(format string, args ...interface{}) {
	aLogEntry.Infof(format, args...)
}

// Warn is a wrapper for the logrus.Warn function.
func Warn(args ...interface{}) {
	aLogEntry.Warn(args...)
}

// Warnf is a wrapper for the logrus.Warnf function.
func Warnf(format string, args ...interface{}) {
	aLogEntry.Warnf(format, args...)
}

// Error is a wrapper for the logrus.Error function.
func Error(args ...interface{}) {
	aLogEntry.Error(args...)
}

// Errorf is a wrapper for the logrus.Errorf function.
func Errorf(format string, args ...interface{}) {
	aLogEntry.Errorf(format, args...)
}

// Debug is a wrapper for the logrus.Debug function.
func Debug(args ...interface{}) {
	aLogEntry.Debug(args...)
}

// Debugf is a wrapper for the logrus.Debugf function.
func Debugf(format string, args ...interface{}) {
	aLogEntry.Debugf(format, args...)
}

// Panic is a wrapper for the standard log.Panic function.  One day it will do magic!
func Panic(args ...interface{}) {
	aLogEntry.Panic(args...)
}

// Panicf is a wrapper for the standard log.Panicf function.  One day it will do magic!
func Panicf(format string, args ...interface{}) {
	aLogEntry.Panicf(format, args...)
}

// Fatal is a wrapper for the standard log.Fatal function.  One day it will do magic!
// Calls to Fatal should only be made for setup errors or otherwise dire misconfigurations - for
// more regular "WTF!? Let's get out of here..." situations you should use Panic
func Fatal(args ...interface{}) {
	aLogEntry.Fatal(args...)
}

// Fatalf is a wrapper for the standard log.Fatalf function.  One day it will do magic!
// Calls to Fatalf should only be made for setup errors or otherwise dire misconfigurations - for
// more regular "WTF!? Let's get out of here..." situations you should use Panicf
func Fatalf(format string, args ...interface{}) {
	aLogEntry.Fatalf(format, args...)
}

// RequestID returns the request ID from a GinLog entry
func (e Entry) RequestID() (requestID string) {
	if rID, ok := e.FieldSet[GINRequestID]; ok {
		requestID, _ = rID.(string)
	}
	return
}

// SetLogLevel sets the minimum Level to be logged
func (e *Entry) SetLogLevel(newLevel Level) (oldLevel Level) {
	oldLevel = Level(e.Logger.Level)
	e.Logger.Level = logrus.Level(newLevel)
	return
}

// LogStack logs the calling stack, with the associated message, in the range specified.
func (e Entry) LogStack(message string, from, to int) {
	indent := ""
	// Start from outermost caller, no need to fix off-by-one since we want caller of caller
	for i := to; i > from; i-- {
		_, fn, line, ok := runtime.Caller(i)
		if ok {
			e.Warnf("%s%s %s [%d]", message, indent, stripFnPrefix(fn), line)
			indent += " "
		}
	}
}

// Customer is like log.Info, but for logging customer-visible messages
func (e Entry) Customer(args ...interface{}) {
	e.WithField("visibility", "customer").Info(args...)
}

// Customerf is like log.Printf, for logging customer-visible messages
func (e Entry) Customerf(format string, args ...interface{}) {
	e.WithField("visibility", "customer").Infof(format, args...)
}

// Fatal is like log.Fatal, but will include the stack
func (e Entry) Fatal(args ...interface{}) {
	e.LogStack("Fatal error occurred at:", 1, 6) // Log some calling stack so we know how we got here.
	e.Entry.WithFields(e.FieldSet).Fatal(args...)
}

// Fatalf is like log.Fatalf, for logging when things are so screwed we have to quit the program
func (e Entry) Fatalf(format string, args ...interface{}) {
	e.LogStack("Fatal error occurred at:", 1, 6) // Log some calling stack so we know how we got here.
	e.Entry.WithFields(e.FieldSet).Fatalf(format, args...)
}

// Panic is like log.Panic, but will include the stack
func (e Entry) Panic(args ...interface{}) {
	e.Entry.WithFields(e.FieldSet).Panic(args...)
}

// Panicf is like log.Panicf, for logging when things are unrecoverable
func (e Entry) Panicf(format string, args ...interface{}) {
	e.Entry.WithFields(e.FieldSet).Panicf(format, args...)
}

// Error is like log.Error, but will include the stack
func (e Entry) Error(args ...interface{}) {
	e.Entry.WithFields(e.FieldSet).Error(args...)
}

// Errorf is like log.Printf, for logging errors, with a stack trace
func (e Entry) Errorf(format string, args ...interface{}) {
	e.Entry.WithFields(e.FieldSet).Errorf(format, args...)
}

// Warn is like log.Warn, but will include the stack
func (e Entry) Warn(args ...interface{}) {
	e.Entry.WithFields(e.FieldSet).Warn(args...)
}

// Warnf is like log.Printf, for logging customer-visible messages
func (e Entry) Warnf(format string, args ...interface{}) {
	e.Entry.WithFields(e.FieldSet).Warnf(format, args...)
}

// Info is a wrapper for the logrus.Info function.
func (e Entry) Info(args ...interface{}) {
	e.Entry.WithFields(e.FieldSet).Info(args...)
}

// Infof is a wrapper for the logrus.Infof function.
func (e Entry) Infof(format string, args ...interface{}) {
	e.Entry.WithFields(e.FieldSet).Infof(format, args...)
}

// Debug is a wrapper for the logrus.Debug function.
func (e Entry) Debug(args ...interface{}) {
	e.Entry.WithFields(e.FieldSet).Debug(args...)
}

// Debugf is a wrapper for the logrus.Debugf function.
func (e Entry) Debugf(format string, args ...interface{}) {
	e.Entry.WithFields(e.FieldSet).Debugf(format, args...)
}

// GinLog is used for logging within the Gin request context allowing log messages for
// a single request to be associated
func GinLog(c *gin.Context) *Entry {
	var requestID string
	if c != nil {
		requestID = c.GetString(GINRequestID)
	}
	if requestID == "" {
		return aLogEntry
	}
	return NewEntry(aLogger).WithFields(Fields{
		GINRequestID: requestID,
	})
}

// GinLogger is a replacement for the standard default gin logging solution
// loosely based on 'Ginerus', which is MIT licensed
func GinLogger(logRequestStartFinish bool) gin.HandlerFunc {
	return func(c *gin.Context) {
		start := time.Now()
		clientIP := c.ClientIP()
		path := c.Request.URL.Path
		method := c.Request.Method
		timeFormatted := start.Format(time.RFC3339Nano)
		ctx := WithRequestID(context.Background())
		requestID := ctx.Value(reqIDKey).(string)
		c.Header("X-API-Request", requestID)
		c.Set(GINRequestID, requestID)
		c.Set(ALogContext, ctx)

		if logRequestStartFinish {
			msg := fmt.Sprintf(`%s %s '%s %s' started processing request`, timeFormatted, clientIP, method, path)

			WithContext(ctx).WithFields(Fields{
				"method": method,
				"path":   path,
				"ip":     clientIP,
			}).Info(msg)
		}

		defer func() {
			end := time.Now()
			latency := end.Sub(start)
			userAgent := c.Request.UserAgent()

			statusCode := c.Writer.Status()
			comment := c.Errors.String()

			timeFormatted = end.Format(time.RFC3339Nano)

			msg := fmt.Sprintf("%s %s '%s %s' finished with %d after %s", timeFormatted, clientIP, method, path, statusCode, latency)

			logrec := WithContext(ctx).WithFields(Fields{
				"time":       timeFormatted,
				"latency":    latency,
				"comment":    comment,
				"status":     statusCode,
				"method":     method,
				"path":       path,
				"user-agent": userAgent,
				"ip":         clientIP,
			})
			if statusCode > 499 {
				logrec.Error(msg)
			} else if statusCode > 399 {
				logrec.Warn(msg)
			} else if logRequestStartFinish {
				logrec.Info(msg)
			}
			ctx.Done()
		}()

		c.Next()
	}
}
