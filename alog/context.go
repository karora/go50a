package alog

import (
	"context"
	"time"

	"github.com/gin-gonic/gin"
)

type (
	// Context is a context with logging capability
	Context interface {
		Fatal(args ...interface{})
		Fatalf(fmt string, args ...interface{})
		Error(args ...interface{})
		Errorf(fmt string, args ...interface{})
		Warn(args ...interface{})
		Warnf(fmt string, args ...interface{})
		Info(args ...interface{})
		Infof(fmt string, args ...interface{})
		Debug(args ...interface{})
		Debugf(fmt string, args ...interface{})
		WithField(key string, value interface{}) *Entry
		WithFields(Fields) *Entry
		WithContext(context.Context) Context

		// LogStack will return a stacktrace omitting `from` levels of caller which is `to` levels deep
		// e.g. `LogStack("message", 2, 5)` will omit the immediate caller and then provide 5 levels
		LogStack(message string, from, to int)

		// Deadline returns the time when work done on behalf of this context
		// should be canceled. Deadline returns ok==false when no deadline is
		// set. Successive calls to Deadline return the same results.
		Deadline() (deadline time.Time, ok bool)

		// Done returns a channel that's closed when work done on behalf of this
		// context should be canceled. Done may return nil if this context can
		// never be canceled. Successive calls to Done return the same value.
		Done() <-chan struct{}

		// If Done is not yet closed, Err returns nil.
		// If Done is closed, Err returns a non-nil error explaining why:
		// Canceled if the context was canceled
		// or DeadlineExceeded if the context's deadline passed.
		// After Err returns a non-nil error, successive calls to Err return the same error.
		Err() error

		// Value returns the value associated with this context for key, or nil
		// if no value is associated with key. Successive calls to Value with
		// the same key returns the same result.
		Value(key interface{}) interface{}
	}
)

type logContext struct {
	ctx  context.Context
	cxID string
}

// WithLogging returns a logging context
func WithLogging(ctx context.Context) Context {
	if ctx == nil {
		LogStack("WithLogging called with nil context", 1, 4)
	}

	lc := &logContext{
		ctx: ctx,
	}
	if ctx == nil {
		lc.cxID = "none"
		lc.ctx = context.TODO()
	} else {
		var inC interface{}
		inC = ctx
		ginC, ok := inC.(*gin.Context)
		if ok {
			lc.cxID = ginC.GetString(GINRequestID)
		} else {
			dbC, ok := inC.(*logContext)
			if ok {
				lc.cxID = dbC.cxID
			}
		}
		if lc.cxID == "" {
			lc.cxID = UniqueTag()
		}
	}

	return lc
}

// Deadline returns the time when work done on behalf of this context
// should be canceled. Deadline returns ok==false when no deadline is
// set. Successive calls to Deadline return the same results.
func (dbc logContext) Deadline() (deadline time.Time, ok bool) {
	return dbc.ctx.Deadline()
}

// Done returns a channel that's closed when work done on behalf of this
// context should be canceled. Done may return nil if this context can
// never be canceled. Successive calls to Done return the same value.
func (dbc logContext) Done() <-chan struct{} {
	return dbc.ctx.Done()
}

// If Done is not yet closed, Err returns nil.
// If Done is closed, Err returns a non-nil error explaining why:
// Canceled if the context was canceled
// or DeadlineExceeded if the context's deadline passed.
// After Err returns a non-nil error, successive calls to Err return the same error.
func (dbc logContext) Err() error {
	return dbc.ctx.Err()
}

// Value returns the value associated with this context for key, or nil
// if no value is associated with key. Successive calls to Value with
// the same key returns the same result.
func (dbc logContext) Value(key interface{}) interface{} {
	if _, ok := key.(reqIDType); ok {
		return dbc.cxID
	}
	return dbc.ctx.Value(key)
}

// func (dbc logContext) GetContext() context.Context {
// 	return dbc.ctx
// }

// Fatal simplifies the interface to alog.Fatal
func (dbc logContext) Fatal(args ...interface{}) {
	WithContext(dbc.ctx).Error(args...)
}

// Fatalf simplifies the interface to alog.Fatalf
func (dbc logContext) Fatalf(fmt string, args ...interface{}) {
	WithContext(dbc.ctx).Errorf(fmt, args...)
}

// Error simplifies the interface to alog.Error
func (dbc logContext) Error(args ...interface{}) {
	WithContext(dbc.ctx).Error(args...)
}

// Errorf simplifies the interface to alog.Errorf
func (dbc logContext) Errorf(fmt string, args ...interface{}) {
	WithContext(dbc.ctx).Errorf(fmt, args...)
}

// Warn simplifies the interface to alog.Warn
func (dbc logContext) Warn(args ...interface{}) {
	WithContext(dbc.ctx).Warn(args...)
}

// Warnf simplifies the interface to alog.Warnf
func (dbc logContext) Warnf(fmt string, args ...interface{}) {
	WithContext(dbc.ctx).Warnf(fmt, args...)
}

// Info simplifies the interface to alog.Info
func (dbc logContext) Info(args ...interface{}) {
	WithContext(dbc.ctx).Info(args...)
}

// Infof simplifies the interface to alog.Infof
func (dbc logContext) Infof(fmt string, args ...interface{}) {
	WithContext(dbc.ctx).Infof(fmt, args...)
}

// Debug simplifies the interface to alog.Debug
func (dbc logContext) Debug(args ...interface{}) {
	WithContext(dbc.ctx).Debug(args...)
}

// Debugf simplifies the interface to alog.Debugf
func (dbc logContext) Debugf(fmt string, args ...interface{}) {
	WithContext(dbc.ctx).Debugf(fmt, args...)
}

// LogStack logs the calling stack, with the associated message, in the range specified.
func (dbc logContext) LogStack(message string, from, to int) {
	WithContext(dbc.ctx).LogStack(message, from+1, to+1)
}

func (dbc logContext) WithFields(f Fields) *Entry {
	return WithContext(dbc.ctx).WithFields(f)
}

func (dbc logContext) WithField(key string, value interface{}) *Entry {
	return dbc.WithFields(Fields{key: value})
}

func (dbc *logContext) WithContext(ctx context.Context) Context {
	if dbc.cxID == "" && ctx == nil {
		return &logContext{
			cxID: "none",
			ctx:  context.Background(),
		}
	}
	if ctx != nil {
		dbc.ctx = ctx
	}
	var inC interface{} = ctx
	dbC, ok := inC.(*logContext)
	if ok {
		dbc.cxID = dbC.cxID
	} else {
		ginC, ok := inC.(*gin.Context)
		if ok {
			dbc.cxID = ginC.GetString(GINRequestID)
		}
	}
	if dbc.cxID == "" {
		dbc.cxID = UniqueTag()
	}
	return dbc
}
