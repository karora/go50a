package alog

import (
	"errors"
	"testing"
)

// Testing
//
// Logrus has a built in facility for asserting the presence of log messages. This is implemented through the test hook and provides:
//
// decorators for existing logger (test.NewLocal and test.NewGlobal) which basically just add the test hook
// a test logger (test.NewNullLogger) that just records log messages (and does not output any):
//   logger, hook := NewNullLogger()
//   logger.Error("Hello error")
//
//   assert.Equal(1, len(hook.Entries))
//   assert.Equal(logrus.ErrorLevel, hook.LastEntry().Level)
//   assert.Equal("Hello error", hook.LastEntry().Message)
//
//   hook.Reset()
//   assert.Nil(hook.LastEntry())

// Test for filename stripping code.
func Test_stripFnPrefix(t *testing.T) {
	result := stripFnPrefix("/home/karora/go/src/gopkg.in/mgo.v2/bson")
	if result != "gopkg.in/mgo.v2/bson" {
		t.Errorf("Badly stripped result: '%s'", result)
	}

	result = stripFnPrefix("/home/karora/go/src/gitlab.com/karora/go50a/alog")
	if result != "karora/go50a/alog" {
		t.Errorf("Badly stripped result: '%s'", result)
	}

	result = stripFnPrefix("/home/karora/projects/mimble/mumble/drive.go")
	if result != "/home/karora/projects/mimble/mumble/drive.go" {
		t.Errorf("Badly stripped result: '%s'", result)
	}
}

func TestUniqueTag(t *testing.T) {
	result := UniqueTag()
	Info("Unique tag is: ", result)
	if len(result) < 40 {
		t.Errorf("Unique tag '%s' does not seem sufficiently unique!", result)
	}
}

func TestWithFields(t *testing.T) {
	myFields := &Fields{
		"myField":  "myFieldValue",
		"herField": "herFieldValue",
	}
	WithFields(*myFields).Info("Log message tagged with myField and with herField")
}

func TestPanic(t *testing.T) {
	defer func() {
		if r := recover(); r != nil {
			Info("Panic occurred as planned - Don't Panic!")
		}
	}()
	Panic("OMG!!! What is happening!  Panic!")
	t.Error("Failed to panic!")
}

func TestPanicf(t *testing.T) {
	defer func() {
		if r := recover(); r != nil {
			Info("Panic(formatted) occurred as planned - Don't Panic!")
		}
	}()
	Panicf("OMG!!! What does this number mean: %d!  Panic!", 42)
	t.Error("Failed to panic!")
}

func TestCustomerf(t *testing.T) {
	Customerf("I'm a customer visible log message containing '%s' fields.", "extra")
}

func TestCustomer(t *testing.T) {
	loggeryThing := New()
	loggeryThing.WithError(errors.New("I'm a new error using some internal Logrus thing!"))
}

func TestDebug(t *testing.T) {
	Debug("I'm a very super-boring debugging message.")
}

func TestDebugf(t *testing.T) {
	Debugf("I'm a very super-boring debugging message #%03d.", 2)
}

func TestWarn(t *testing.T) {
	Warn("I'm a very super-weird warning message.")
}

func TestWarnf(t *testing.T) {
	Warnf("I'm a very super-strange warning message #%03d.", 2)
}
