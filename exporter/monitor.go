package exporter

import (
	"fmt"

	"github.com/gin-gonic/gin"
	"github.com/prometheus/client_golang/prometheus/promhttp"

	"gitlab.com/karora/go50a/alog"
	"gitlab.com/karora/go50a/scraper"
	"gitlab.com/karora/go50a/settings"
)

// Run configures the exporter and starts the thread
func Run() {
	alog.Info("G50a exporter is starting.")

	scraper.InitScraper()
	settings.SubscribeToReload(scraper.InitScraper)

	exporterDone := make(chan struct{})
	scraperDone := make(chan struct{})

	go Exporter(exporterDone)
	go scraper.Scraper(scraperDone, nil)

	// Will exit when either of those channels are closed.  We're not fussy!
	select {
	case <-exporterDone:
	case <-scraperDone:
	}
	alog.Info("G50a exporter is exiting.")
}

// Exporter is the thread that services the exporter
func Exporter(finished chan struct{}) {
	defer close(finished)

	gin.SetMode(gin.ReleaseMode)
	router := gin.New()
	router.Use(gin.Recovery(), settings.CheckSourceIPRange(settings.AllowedMetricIPs))
	router.GET("/metrics", gin.WrapH(promhttp.Handler()))

	listenPort := settings.GetInt(settings.ServerListenPort)
	listenAddress := fmt.Sprint(":", listenPort)

	alog.Info("G50 exporter will listen on ", listenPort)
	err := router.Run(listenAddress)
	if err != nil {
		alog.Info("G50 exporter exiting with error: ", err.Error())
	} else {
		alog.Info("G50 exporter is exiting.")
	}
}
