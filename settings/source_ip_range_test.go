package settings

import (
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
)

func TestIpInSubnet(t *testing.T) {
	if !isIpInSubnet("192.168.122.17", varMap{"192.168.122.17": true}) {
		t.Error("Couldn't find IP address in varMap")
	}
	if !isIpInSubnet("192.168.122.17", varMap{"192.168.122": true}) {
		t.Error("Couldn't find IP address in varMap of /24 subnet")
	}
	if !isIpInSubnet("192.168.122.17", varMap{"192.168": true}) {
		t.Error("Couldn't find IP address in varMap of /16 subnet")
	}

	w := httptest.NewRecorder()
	c, r := gin.CreateTestContext(w)
	_ = LoadFromJSON([]byte(`{"ips": {"192.0.2.1": "athy.mcmillan.net.nz"}}`))
	r.HandleMethodNotAllowed = true
	c.Request = httptest.NewRequest("GET", "/", nil)
	validateSourceIP(c, "ips")
	if c.Errors != nil {
		t.Error("Couldn't find IP address in varMap")
	}

	_ = LoadFromJSON([]byte(`{"ips": {"192.0.0.0": "athy.mcmillan.net.nz"}}`))
	validateSourceIP(c, "ips")
	if c.Errors == nil {
		t.Error("Didn't exclude IP address we should have")
	}
}
