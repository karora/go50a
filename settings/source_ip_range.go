package settings

import (
	"errors"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	"gitlab.com/karora/go50a/alog"
)

type varMap map[string]interface{}

// CheckSourceIPRange returns a middleware which will confirm the IP address is allowed, or abort with 403
func CheckSourceIPRange(settingName string) gin.HandlerFunc {
	return func(c *gin.Context) {
		validateSourceIP(c, settingName)
		c.Next()
	}
}

func validateSourceIP(c *gin.Context, settingName string) {
	// Can't cast as varMap here - it seems all we have to use the raw type
	validIPs, ok := GetObject(settingName).(map[string]interface{})
	if ok && isIpInSubnet(c.ClientIP(), validIPs) {
		c.Next()
		return
	}

	alog.Infof("Refusing connection for `%s` from invalid Source IP `%s`", settingName, c.ClientIP())
	_ = c.AbortWithError(http.StatusForbidden, errors.New("403 Forbidden"))
}

func isIpInSubnet(ip string, validIPs varMap) bool {
	if validIPs[ip] != nil {
		return true
	}
	// Poor-man's subnet handling for 192.168.122, 172.17 or 10.3 etc
	requestSource := strings.SplitN(ip, ".", 4)
	if len(requestSource) == 4 {
		if validIPs[requestSource[0]] != nil {
			return true
			// } else {
			// 	alog.Infof("Valid source IPs are:\n%v\nDoes not contain '%s'", validIPs, requestSource[0])
		}
		if validIPs[strings.Join(requestSource[0:2], ".")] != nil {
			return true
			// } else {
			// 	alog.Infof("Valid source IPs are:\n%v\nDoes not contain '%s'", validIPs, strings.Join(requestSource[0:2], "."))
		}
		if validIPs[strings.Join(requestSource[0:3], ".")] != nil {
			return true
			// } else {
			// 	alog.Infof("Valid source IPs are:\n%v\nDoes not contain '%s'", validIPs, strings.Join(requestSource[0:3], "."))
		}
	} else {
		alog.Warnf("Request source IP is %v which is length %d", requestSource, len(requestSource))
	}
	return false
}
