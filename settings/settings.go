package settings

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/karora/go50a/alog"
)

// Settings contains the configuration of things using this library
type Settings map[string]interface{}

// Constants for various settings
const (
	debugMode        = "debug_mode" // This one is private, it can only be manipulated through the setter or config
	ServerListenPort = "listen_port"
	ServerPublicRoot = "server_public_root"
	LogFileName      = "log_filename"
	LogToStderr      = "log_to_stderr"
	LogLevel         = `log_level`
	G50Address       = `g50_address`
	G50Zones         = `g50_zones`
	ScrapeInterval   = `scrape_interval`
	AllowedMetricIPs = "allowed_metrics_ips"
)

var settings = make(Settings, 10)
var configFileName string

// ReloadHook functions are called on SIGHUP
type ReloadHook func()

var hooks []ReloadHook
var c chan os.Signal

func waitForHUP() {

	signal.Notify(c, syscall.SIGHUP)

	for true {
		_ = <-c

		alog.Info("Received SIGHUP, reloading configuration from ", configFileName)
		err := LoadFromFile(configFileName)
		if err != nil {
			alog.Warn("Error reloading configuration: ", err)
		} else if hooks != nil {
			alog.Info("Configuration reloaded, notifying subscribers.")
			for _, hook := range hooks {
				hook()
			}
			alog.Info("Configuration reload completed.")
		}
	}
}

// SubscribeToReload will call back to your hook whenever the settings are reloaded
func SubscribeToReload(hook ReloadHook) {
	if hooks == nil {
		hooks = make([]ReloadHook, 0, 10)
	}
	hooks = append(hooks, hook)
}

// LoadFromJSON will load the application settings from a byte array.
func LoadFromJSON(jsonContent []byte) error {
	newSettings := make(Settings, 20)
	err := json.Unmarshal(jsonContent, &newSettings)
	if err != nil {
		alog.Info("Error while parsing settings JSON: ", err)
		return err
	}
	settings = newSettings
	if GetString(LogFileName) != "" {
		alog.SetOutputFile(GetString(LogFileName))
	}
	return nil
}

// LoadFromFile will load the application settings from a JSON file.
func LoadFromFile(path string) error {
	if path == "" {
		return nil
	}
	if path[0:1] == "~" {
		path = os.Getenv("HOME") + path[1:]
	}
	configFileName = path
	content, err := ioutil.ReadFile(configFileName)
	if err != nil {
		alog.Info("Error while reading settings file: ", err)
		return err
	}
	if c == nil {
		c = make(chan os.Signal, 1)
		go waitForHUP()
	}
	return LoadFromJSON(content)
}

// Debug wraps the boolean value of a debug setting and is special in that it is settable
func Debug() bool {
	return GetBool(debugMode)
}

// SetDebug sets the value of the debug mode, returning the prior value
func SetDebug(new bool) (old bool) {
	old = Debug()
	settings[debugMode] = new
	return
}

// Exists returns an indication if the setting exists at all
func Exists(name string) bool {
	_, ok := settings[name]
	return ok
}

// GetString the value of a setting as a string
func GetString(name string) string {
	value, ok := settings[name]
	if !ok {
		value = ""
		settings[name] = ""
	}
	result, ok := value.(string)
	if !ok {
		alog.Info("Non-string setting '", name, "' being converted to: '", value, "'")
		result = fmt.Sprint(value)
		alog.Infof("Non-string setting '%s' now: %T %v", name, result, result)
	}
	return result
}

// GetFloat64 like GetString, but converts the value to an Float64 (JSON numeric)
func GetFloat64(name string) float64 {
	var result float64
	var err error
	rawVal, ok := settings[name]
	if ok {
		result, ok = rawVal.(float64)
		if !ok {
			err = errors.New("value '" + fmt.Sprint(rawVal) + "' for '" + name + "' is not numeric")
		}
	} else {
		settings[name] = result
	}
	if err != nil {
		alog.Warnf("Setting '%s' is %q but a number was expected! Error: "+err.Error(), name, rawVal)
	}
	return result
}

// GetInt returns the setting as an integer
func GetInt(name string) int {
	return int(GetFloat64(name))
}

// GetBool returns the setting as a boolean
func GetBool(name string) bool {
	value, ok := settings[name]
	if !ok {
		settings[name] = false
		return false
	}
	result, ok := value.(bool)
	if !ok {
		alog.Warn("Retrieving setting for '", name, "', expected a bool, got: ", value)
		settings[name] = result
	}
	return result
}

// GetObject returns the setting as an interface{}
func GetObject(name string) interface{} {
	value, ok := settings[name]
	if !ok {
		alog.Info("Retrieving setting for '", name, "' as Object which is not present.")
		settings[name] = value
	}
	return value
}

// SetObject sets the setting to a new value
func SetObject(name string, newValue interface{}) (old interface{}) {
	old = settings[name]
	settings[name] = newValue
	return
}
