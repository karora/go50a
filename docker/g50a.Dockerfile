# Start from scratch to create an absolutely minimal-sized image
FROM scratch AS go50a-exporter

ARG ARCH=amd64
COPY g50a-${ARCH} /g50a

COPY g50a-config.json /etc/g50a/g50a.config

# We specify the config filename to be mounted externally in that place
CMD ["/g50a", "-config", "/etc/g50a/g50a.config", "-exporter"]