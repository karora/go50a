#!/usr/bin/make
#
# Make the Go50a things
#
#

GO      = go
GODOC   = godoc
GOFMT   = gofmt
TIMEOUT = 15
V = 0
Q = $(if $(filter 1,$V),,@)
M = $(shell printf "\033[34;1m▶\033[0m")
NOW=$(shell date +%Y%m%dT%H%M%S)

all: g50a

g50a:  app/*.go g50api/*.go exporter/*.go alog/*.go settings/*.go go.mod
	CGO_ENABLED=0 GOOS=linux GOARCH=arm64 bash -c "go build -a -installsuffix cgo -o g50a-\$$GOARCH ./app"
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64 GOAMD64=v3 bash -c "go build -a -installsuffix cgo -o g50a-\$$GOARCH ./app"
	cp g50a-amd64 g50a

.PHONY: clean
clean:
	rm -rf g50a g50a-*

# Dependency management
.PHONY: modules
modules:
	GOPRIVATE=gitlab.com/karora/* go get -u `grep '^[[:space:]]' go.mod | grep -v indirect | cut -f1 -d' '`
	go mod tidy