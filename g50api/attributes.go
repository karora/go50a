package g50api

func DefaultZoneRequest(zone int) Zone {
	return Zone{
		Zone:      zone,
		Drive:     "*",
		Mode:      "*",
		SetTemp:   "*",
		InletTemp: "*",
		FanSpeed:  "*",
		Vane:      "*",
		// Bulk:      "*",
	}
}

func BulkZoneRequest(zone int) Zone {
	return Zone{
		Zone: zone,
		Bulk: "*",
	}
}

func FullZoneRequest(zone int) Zone {
	return Zone{
		Zone:          zone,
		Bulk:          "*",
		Drive:         "*",
		Mode:          "*",
		SetTemp:       "*",
		InletTemp:     "*",
		FanSpeed:      "*",
		Vane:          "*",
		RemoCon:       "*",
		DriveItem:     "*",
		ModeItem:      "*",
		SetTempItem:   "*",
		FilterItem:    "*",
		EnergyControl: "*",
		HeatMax:       "*",
		CoolMin:       "*",
		AutoMin:       "*",
		AutoMax:       "*",
		CoolMax:       "*",
		HeatMin:       "*",
		FilterSign:    "*",
		Model:         "*",
		ModeStatus:    "*",
		IcKind:        "*",
		TempDetail:    "*",
		Ventilation:   "*",
	}
}

func SystemDataRequest() *SystemData {
	return &SystemData{
		IPPowerCount:   "*",
		PortPowerCount: "*",
		IPDemand:       "*",
		PortDemand:     "*",
	}
}

func SummerTimeRequest() *SummerTime {
	return &SummerTime{
		CountryCode: "*",
		Month1:      "*",
		Day1:        "*",
		Hour1:       "*",
		Minute1:     "*",
		ShiftMin1:   "*",
		Month2:      "*",
		Day2:        "*",
		Hour2:       "*",
		Minute2:     "*",
		ShiftMin2:   "*",
	}
}

func ClockRequest() *Clock {
	return &Clock{
		Year:   "*",
		Month:  "*",
		Day:    "*",
		Hour:   "*",
		Minute: "*",
		Second: "*",
	}
}

const (
	RequestTypeSet = "setRequest"
	RequestTypeGet = "getRequest"

	QueryTypeZone           = "Mnet"
	QueryTypeSystemData     = "SystemData"
	QueryTypeSummerTime     = "SummerTime"
	QueryTypeClock          = "Clock"
	QueryTypeFilterStatus   = "FilterStatusList"
	QueryTypeAlarmStatus    = "AlarmStatusList"
	QueryTypeDdcAlarmStatus = "DdcAlarmStatusList"
)

func RequestTypes() []string {
	return []string{
		RequestTypeSet,
		RequestTypeGet,
	}
}

func QueryTypes() map[string]string {
	return map[string]string{
		QueryTypeZone:           "basic",
		QueryTypeSystemData:     "basic",
		QueryTypeSummerTime:     "basic",
		QueryTypeClock:          "basic",
		QueryTypeFilterStatus:   "status",
		QueryTypeAlarmStatus:    "status",
		QueryTypeDdcAlarmStatus: "status",
	}
}
