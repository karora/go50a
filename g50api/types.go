package g50api

import (
	"encoding/xml"
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/karora/go50a/settings"
)

type Zone struct {
	Zone          int        `xml:"Group,attr,omitempty"`
	Bulk          string     `xml:"Bulk,attr,omitempty"`
	SetTemp       string     `xml:"SetTemp,attr,omitempty"`
	InletTemp     string     `xml:"InletTemp,attr,omitempty"`
	Drive         string     `xml:"Drive,attr,omitempty"`
	Mode          string     `xml:"Mode,attr,omitempty"`
	FanSpeed      string     `xml:"FanSpeed,attr,omitempty"`
	Vane          string     `xml:"AirDirection,attr,omitempty"`
	RemoCon       string     `xml:"RemoCon,attr,omitempty"`
	DriveItem     string     `xml:"DriveItem,attr,omitempty"`
	ModeItem      string     `xml:"ModeItem,attr,omitempty"`
	SetTempItem   string     `xml:"SetTempItem,attr,omitempty"`
	FilterItem    string     `xml:"FilterItem,attr,omitempty"`
	EnergyControl string     `xml:"EnergyControl,attr,omitempty"`
	CoolMax       string     `xml:"CoolMax,attr,omitempty"`
	CoolMin       string     `xml:"CoolMin,attr,omitempty"`
	HeatMax       string     `xml:"HeatMax,attr,omitempty"`
	HeatMin       string     `xml:"HeatMin,attr,omitempty"`
	AutoMax       string     `xml:"AutoMax,attr,omitempty"`
	AutoMin       string     `xml:"AutoMin,attr,omitempty"`
	FilterSign    string     `xml:"FilterSign,attr,omitempty"`
	Model         string     `xml:"Model,attr,omitempty"`
	ModeStatus    string     `xml:"ModeStatus,attr,omitempty"`
	IcKind        string     `xml:"IcKind,attr,omitempty"`
	TempDetail    string     `xml:"TempDetail,attr,omitempty"`
	Ventilation   string     `xml:"Ventilation,attr,omitempty"`
	QueryType     *QueryType `xml:"querytype,omitempty"`
}

func appendIf(sb *strings.Builder, test string, append string) {
	if test != "" {
		(*sb).WriteString(append)
	}
}

func (z Zone) String() string {
	var sb strings.Builder
	var l2 strings.Builder
	if z.Zone != 0 {
		sb.WriteString(fmt.Sprintf("Zone %d: ", z.Zone))
		appendIf(&sb, z.Drive, z.Drive+" ")
		appendIf(&sb, z.Mode, z.Mode+" ")
		if z.SetTemp != "" {
			sb.WriteString(z.SetTemp + "°")
			appendIf(&sb, z.InletTemp, " (In: "+z.InletTemp+"°)")
		}
		appendIf(&sb, z.Mode, ", Fan: "+z.FanSpeed)
		appendIf(&sb, z.Mode, ", Vane: "+z.Vane)

		appendIf(&sb, z.Bulk, "\n        "+z.Bulk)

		appendIf(&l2, z.RemoCon, "Remote: "+z.RemoCon+" ")
		appendIf(&l2, z.HeatMax, "Heat max: "+z.HeatMax+"° ")
		appendIf(&l2, z.CoolMin, "Cool min: "+z.CoolMin+"° ")
		appendIf(&l2, z.AutoMin, "Auto min: "+z.AutoMin+"° ")
		appendIf(&l2, z.AutoMax, "Auto max: "+z.AutoMax+"° ")
		if l2.Len() > 0 {
			sb.WriteString("\n        " + l2.String())
		}
	}
	return sb.String()
}

const (
	drivePos         = 3
	tempPos          = 6
	inletPos         = 12
	fanSpeedPos      = 17
	vaneDirectionPos = 15
)

func (z Zone) GetDrive() (drive int) {
	if len(z.Bulk) > drivePos {
		driveBytes := z.Bulk[drivePos : drivePos+1]
		drive, _ = strconv.Atoi(driveBytes)
		if settings.Debug() {
			fmt.Printf("Drive is %q, a.k.a %q\n", driveBytes, IntToDrive(drive))
		}
		return
	}
	return FanSpeedToInt(z.FanSpeed)
}

func (z Zone) GetTemp() (temp int) {
	if len(z.Bulk) > drivePos {
		tempBytes := z.Bulk[tempPos : tempPos+2]
		intval, _ := strconv.ParseUint(tempBytes, 16, 64)
		temp = int(intval)
		if settings.Debug() {
			fmt.Printf("SetTemp is %q, a.k.a %q\n", tempBytes, IntToTemp(temp))
		}
		return
	}
	temp, _ = strconv.Atoi(z.SetTemp)
	return temp
}

func (z Zone) GetInlet() (temp int) {
	if len(z.Bulk) > drivePos {
		tempBytes := z.Bulk[inletPos : inletPos+2]
		intval, _ := strconv.ParseUint(tempBytes, 16, 64)
		temp = int(float64(intval) / 10)
		if settings.Debug() {
			fmt.Printf("InletTemp is %q, dec %d, a.k.a %q\n", tempBytes, intval, IntToTemp(temp))
		}
		return
	}
	f, _ := strconv.ParseFloat(z.SetTemp, 64)
	temp = int(f)
	return temp
}

func (z Zone) GetFanSpeed() (fanSpeed int) {
	if len(z.Bulk) > fanSpeedPos {
		fanBytes := z.Bulk[fanSpeedPos : fanSpeedPos+1]
		fanSpeed, _ = strconv.Atoi(fanBytes)
		fanSpeed++ // Bulk FanSpeed is 0 = LOW .. 4 = HIGH, but 0 is special for us
		if settings.Debug() {
			fmt.Printf("FanSpeed is %q, a.k.a %q\n", fanBytes, IntToFanSpeed(fanSpeed))
		}
		return
	}
	return FanSpeedToInt(z.FanSpeed)
}

func (z Zone) GetVane() (vaneDirection int) {
	if len(z.Bulk) > fanSpeedPos {
		vaneBytes := z.Bulk[vaneDirectionPos : vaneDirectionPos+1]
		vaneDirection, _ = strconv.Atoi(vaneBytes)
		if settings.Debug() {
			fmt.Printf("VaneDirection is %q, a.k.a %q\n", vaneBytes, IntToVane(vaneDirection))
		}
		return
	}
	return VaneToInt(z.Vane)
}

func IntToTemp(temp int) string {
	if temp == 0 {
		return ""
	}

	return strconv.Itoa(temp)
}

const (
	DriveOn             = "ON"
	DriveOff            = "OFF"
	RemoteControlPermit = "PERMIT"
	RemoteControlDeny   = "PROHIBIT"
)

func DriveToInt(drive string) int {
	if drive == "ON" {
		return 1
	}
	return 0
}

func IntToDrive(in int) string {
	if in == 1 {
		return "ON"
	}
	return "OFF"
}

func FanSpeedToInt(fan string) int {
	// This is off-by-one vs the value in "Bulk" which returns "0" for "LOW"
	switch fan {
	case "LOW":
		return 1
	case "MID2":
		return 2
	case "MID1":
		return 3
	case "HIGH":
		return 4
	}
	return 0
}

func IntToFanSpeed(fan int) string {
	switch fan {
	case 1:
		return "LOW"
	case 2:
		return "MID2"
	case 3:
		return "MID1"
	case 4:
		return "HIGH"
	}
	return ""
}

func VaneToInt(vane string) int {
	switch vane {
	case "VERTICAL":
		return 1
	case "MID2":
		return 2
	case "MID1":
		return 3
	case "HORIZIONTAL":
		return 4
	case "SWING":
		// Actually uses "00" in Bulk
		return 5
	}
	return 0
}

func IntToVane(vane int) string {
	switch vane {
	case 5:
		return "SWING"
	case 1:
		return "VERTICAL"
	case 2:
		return "MID2"
	case 3:
		return "MID1"
	case 4:
		return "HORIZONTAL"
	}
	return ""
}

func ValidMode(mode string) bool {
	valid := map[string]bool{
		"HEAT": true,
		"COOL": true,
		"DRY":  true,
		"AUTO": true,
		"FAN":  true,
	}
	if _, exists := valid[mode]; exists {
		return true
	}
	return false
}

type SystemData struct {
	IPPowerCount   string `xml:"IPPowerCount,attr,omitempty"`
	PortPowerCount string `xml:"PortPowerCount,attr,omitempty"`
	IPDemand       string `xml:"IPDemand,attr,omitempty"`
	PortDemand     string `xml:"PortDemand,attr,omitempty"`
}

type SummerTime struct {
	CountryCode string `xml:"CountryCode,attr,omitempty"`
	Month1      string `xml:"Month1,attr,omitempty"`
	Day1        string `xml:"Day1,attr,omitempty"`
	Hour1       string `xml:"Hour1,attr,omitempty"`
	Minute1     string `xml:"Minute1,attr,omitempty"`
	ShiftMin1   string `xml:"ShiftMin1,attr,omitempty"`
	Month2      string `xml:"Month2,attr,omitempty"`
	Day2        string `xml:"Day2,attr,omitempty"`
	Hour2       string `xml:"Hour2,attr,omitempty"`
	Minute2     string `xml:"Minute2,attr,omitempty"`
	ShiftMin2   string `xml:"ShiftMin2,attr,omitempty"`
}

type Clock struct {
	Year   string `xml:"Year,attr,omitempty"`
	Month  string `xml:"Month,attr,omitempty"`
	Day    string `xml:"Day,attr,omitempty"`
	Hour   string `xml:"Hour,attr,omitempty"`
	Minute string `xml:"Minute,attr,omitempty"`
	Second string `xml:"Second,attr,omitempty"`
}

type QueryType struct {
	XMLName xml.Name
}

type G50Data struct {
	Zones      []Zone      `xml:"Mnet,omitempty"`
	SystemData *SystemData `xml:"SystemData,omitempty"`
	SummerTime *SummerTime `xml:"SummerTime,omitempty"`
	Clock      *Clock      `xml:"Clock,omitempty"`
}

func (g G50Data) String() string {
	var s string
	for i, z := range g.Zones {
		s += z.String()
		if i+1 < len(g.Zones) {
			s += "\n"
		}
	}
	if g.SystemData != nil {
		s += fmt.Sprintf("System Data: %v", *g.SystemData)
	}
	if g.SummerTime != nil {
		s += fmt.Sprintf("DST Setup: %v", *g.SummerTime)
	}
	if g.Clock != nil {
		s += fmt.Sprintf("Clock: %v", *g.Clock)
	}
	return s
}

type rawPacket struct {
	XMLName xml.Name `xml:"Packet"`
	Command string
	Data    G50Data `xml:"DatabaseManager"`
}
