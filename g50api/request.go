package g50api

import (
	"bytes"
	"context"
	"encoding/xml"
	"fmt"
	"io"
	"net/http"

	"gitlab.com/karora/go50a/alog"
	"gitlab.com/karora/go50a/settings"
)

type G50Request struct {
	Type string
	Data G50Data
}

func g50URL() string {
	return "http://" + settings.GetString(settings.G50Address) + "/servlet/MIMEReceiveServlet"
}

func (g G50Request) MarshalReader() io.Reader {
	packet := rawPacket{
		Command: "getRequest",
		Data:    g.Data,
	}
	if g.Type == RequestTypeSet {
		packet.Command = g.Type
	}
	b, err := xml.MarshalIndent(packet, "", "  ")
	if err != nil {
		alog.Fatalf("Unable to marshal XML from %v", packet)
	}
	if settings.Debug() {
		fmt.Println(string(b))
	}
	return bytes.NewReader(b)
}

func (g G50Request) Request(ctx context.Context) (G50Data, error) {
	req, err := http.NewRequestWithContext(ctx, http.MethodPost, g50URL(), g.MarshalReader())
	if err != nil {
		alog.Fatal("Failed to create request: ", err)
	}
	req.Header.Add("User-Agent", "go50a/1.0")
	req.Header.Add("Content-Type", "text/xml")

	client := &http.Client{}
	rsp, err := client.Do(req)
	if err != nil {
		alog.Fatal("Failed to create request: ", err)
	}
	body, err := io.ReadAll(rsp.Body)
	if err != nil {
		alog.Error("Error reading response: ", err)
	}

	if settings.Debug() {
		for k, v := range rsp.Header {
			fmt.Println(k, ": ", v)
		}
		fmt.Println("")
		fmt.Println(string(body))
	}

	rr := &rawPacket{}
	err = xml.Unmarshal(body, rr)
	if err != nil {
		alog.Error("Error parsing XML response: ", err)
	}

	if settings.Debug() {
		for _, zone := range rr.Data.Zones {
			fmt.Println(zone)
			zone.GetDrive()
			zone.GetTemp()
			zone.GetInlet()
			zone.GetFanSpeed()
			zone.GetVane()
		}
	}

	return rr.Data, nil
}
